/*
 * @Description: 防抖
 * @Version: 1.0.0
 * @Author: gyh
 * @Date: 2021-08-18 10:17:38
 * @LastEditors: gyh
 * @LastEditTime: 2021-08-18 11:32:18
 */
const input = document.getElementById('input')
const reg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/

input.addEventListener(
  'keyup',
  debounce(() => {
    console.log(reg.test(input.value) ? '校验成功！' : '请输入正确的邮箱地址')
  }, 300),
)

function debounce(fn, delay = 500) {
  let timer = null
  return function () {
    if (timer) clearTimeout(timer)
    timer = setTimeout(() => {
      fn.apply(this, arguments)
      timer = null
    }, delay)
  }
}
