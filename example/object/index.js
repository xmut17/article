/*
 * @Description: 对象
 * @Version: 1.0.0
 * @Author: gyh
 * @Date: 2021-08-17 22:22:39
 * @LastEditors: gyh
 * @LastEditTime: 2021-08-17 22:23:14
 */
const obj = {
  a: 111,
}
// 不可扩展对象,使用该方法可以让传入的对象禁止添加属性和方法
Object.preventExtensions(obj)
obj.b = 666
// 判断对象是否可扩展
console.log(Object.isExtensible(obj))
// 密封的对象,不可扩展，不能删除，但可以修改
Object.seal(obj)
obj.a = 'hello'
// 判断对象是否密封
console.log(Object.isSealed(obj))
// 冻结的对象,不可扩展，密封，不能修改，访问器属性可写
Object.freeze(obj)
obj.a = 'hello222'
console.log(obj)

let arr = { 0: 'first', 1: 'second', length: 2 }
let a = Array.from(arr)
console.log(a)
let b = Array.prototype.concat.apply([], arr)
console.log(b)
let c = Array.prototype.map.call(arr, item => item)
console.log(c)
let d = Array.prototype.slice.apply(arr)
console.log(d)
