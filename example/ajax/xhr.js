/*
 * @Description: XMLHttpRequest封装
 * @Version: 1.0.0
 * @Author: gyh
 * @Date: 2021-08-10 23:34:48
 * @LastEditors: gyh
 * @LastEditTime: 2021-08-10 23:43:10
 */
function Ajax(method, url, params, contentType) {
  const xhr = new XMLHttpRequest()
  const formData = new FormData()
  Object.keys(params).forEach(k => {
    formData.append(k, params[k])
  })
  return new Promise((resolve, reject) => {
    try {
      xhr.open(method, url, false)
      xhr.setRequestHeader('Content-Type', contentType)
      xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
          if (xhr.status >= 200 && xhr.status < 400) {
            resolve(xhr.responseText)
          } else {
            reject(xhr)
          }
        }
      }
    } catch (err) {
      reject(err)
    }
  })
}
