# 文章

[语雀地址](https://www.yuque.com/fengchuippliang)

## 项目开发总结

[vue3+vite+ts 项目搭建](https://www.yuque.com/fengchuippliang/hqbitu/eb5old)

[IndexedDB](https://www.yuque.com/fengchuippliang/hqbitu/dne00h)

[WebSocket](https://www.yuque.com/fengchuippliang/hqbitu/zy5gdv)

[Uni-app](https://www.yuque.com/fengchuippliang/hqbitu/ny150b)

[大文件切片上传](https://www.yuque.com/fengchuippliang/hqbitu/ogzebq)

[Canvas](https://www.yuque.com/fengchuippliang/hqbitu/cotawg)

[SSR](https://www.yuque.com/fengchuippliang/hqbitu/lioimt)

[Vue 权限路由实现方式](https://www.yuque.com/fengchuippliang/hqbitu/bmclz4)

[Vue 中 Axios 的封装和 API 接口的管理](https://www.yuque.com/fengchuippliang/hqbitu/ot8bss)

## 文章记录

[性能优化与解决方案](https://www.yuque.com/fengchuippliang/lvgqyd/ouk4nz)

[call、apply、bind 的用法](https://www.yuque.com/fengchuippliang/lvgqyd/zrtize)

[解析 Array.prototype.slice.call()](https://www.yuque.com/fengchuippliang/lvgqyd/nmy48f)

[创建对象的各种方式](https://www.yuque.com/fengchuippliang/lvgqyd/htggrb)

[Vue2 与 Vue3 组件通信方式总结](https://www.yuque.com/fengchuippliang/lvgqyd/aygcwf)

## 大前端

[📚 函数式编程](https://www.yuque.com/fengchuippliang/tcp8ed/enesh7)

[📚 异步编程](https://www.yuque.com/fengchuippliang/tcp8ed/le1ttu)

[📚 ECMAScript](https://www.yuque.com/fengchuippliang/tcp8ed/dq56xk)

[📚 TypeScript](https://www.yuque.com/fengchuippliang/tcp8ed/sif2gg)

[📚 性能优化](https://www.yuque.com/fengchuippliang/tcp8ed/pge20d)

[📚 前端工程化](https://www.yuque.com/fengchuippliang/tcp8ed/owioui)

[📚 模块化开发与规范化标准](https://www.yuque.com/fengchuippliang/tcp8ed/kteqxb)

[📚 Vue](https://www.yuque.com/fengchuippliang/tcp8ed/oqgs3x)

## 前端面试

[Javascript 基础知识面试知识点](https://www.yuque.com/fengchuippliang/efzx9v/bvwshu)

[Javascript Web-API 面试知识点](https://www.yuque.com/fengchuippliang/efzx9v/shv20c)

[前端开发环境](https://www.yuque.com/fengchuippliang/efzx9v/tcphef)

[前端运行环境](https://www.yuque.com/fengchuippliang/efzx9v/dg12fi)

[前端框架及项目面试](https://www.yuque.com/fengchuippliang/efzx9v/dla8lg)

[面试题](https://www.yuque.com/fengchuippliang/efzx9v/ykvq25)

## NodeJs

[初识 node](https://www.yuque.com/fengchuippliang/nodejs/hqf6xx)

[MySql](https://www.yuque.com/fengchuippliang/nodejs/zlp82i)

[基本模块](https://www.yuque.com/fengchuippliang/nodejs/kbhqkv)

[Node 项目搭建](https://www.yuque.com/fengchuippliang/nodejs/kfeclg)

[博客项目](https://www.yuque.com/fengchuippliang/nodejs/nmkoch)
